'use strict';
angular.module('model.mention', ['ngResource']);
angular.module('model.mention').factory('modelMention', ['$resource',
    function($resource) {
        var queries = {
            getChart: {
                method: 'GET',
                params: {type: 'chart'}
            },
            getProgress: {
                method: 'GET',
                params: {type: 'progress'},
                isArray: true
            }
        };

        return $resource('assets/models/mentions/:type.json', {}, queries);
    }
]);
