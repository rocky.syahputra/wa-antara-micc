'use strict';
angular.module('view.page.panels.panel-4', []);
angular.module('view.page.panels.panel-4').component('panel4', {
    templateUrl: 'views/pages/panels/panel-4/panel-4.html',
    controller: [
        function Panel4Controller() {
            var $ctrl = this;
            $ctrl.onInit = function () {
                console.log('dd');
                $ctrl.addClass('outer-element')
            };
        }
    ]
});
