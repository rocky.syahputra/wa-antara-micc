'use strict';
angular.module('view.page.panels', [
    'view.page.panels.panel-1',
    'view.page.panels.panel-2',
    'view.page.panels.panel-3',
    'view.page.panels.panel-4',
    'view.page.panels.panel-5',
    'view.page.panels.panel-6'/*,
    'view.page.panels.panel-7',
    'view.page.panels.panel-8'*/
]);
angular.module('view.page.panels').component('panels', {
    templateUrl: 'views/pages/panels/panels.html',
    controller: [function PanelsController() {
            var $ctrl = this;
        }
    ]
});
