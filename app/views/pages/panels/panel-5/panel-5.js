'use strict';
angular.module('view.page.panels.panel-5', []);
angular.module('view.page.panels.panel-5').component('panel5', {
    templateUrl: 'views/pages/panels/panel-5/panel-5.html',
    controller: [
        function Panel5Controller() {
            var $ctrl = this;
            $ctrl.onInit = function () {
                console.log('dd');
                $ctrl.addClass('outer-element')
            };
        }
    ]
});
