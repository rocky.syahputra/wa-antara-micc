'use strict';
angular.module('view.page.panels.panel-2', []);
angular.module('view.page.panels.panel-2').component('panel2', {
    templateUrl: 'views/pages/panels/panel-2/panel-2.html',
    controller: [
        function Panel2Controller() {
            var $ctrl = this;
            $ctrl.onInit = function () {
                console.log('dd');
                $ctrl.addClass('outer-element')
            };
        }
    ]
});
