'use strict';
angular.module('view.page.panels.panel-1', []);
angular.module('view.page.panels.panel-1').component('panel1', {
    templateUrl: 'views/pages/panels/panel-1/panel-1.html',
    controller: ['coreHelperChart', 'coreHelperColor', 'modelMention',
        function Panel1Controller(coreHelperChart, coreHelperColor, modelMention) {
            var $ctrl = this;
            $ctrl.model = {
                mention: {}
            };
            $ctrl.$onInit = function () {
                // Get mention data
                $ctrl.model.mention.chart = modelMention.getChart(null, $ctrl.onMentionChart);
                $ctrl.model.mention.progress = modelMention.getProgress(null, $ctrl.onMentionProgress);
            };
            $ctrl.onMentionChart = function(chart) {
                // Add configurations for each line
                $ctrl.model.mention.chart.datasets = coreHelperChart.createDatasets(chart.data);
                console.log($ctrl.model.mention);
            };
            $ctrl.onMentionProgress = function(progress) {
                $ctrl.model.mention.progress = progress[0];
                console.log($ctrl.model.mention);
            };
            $ctrl.onLineChartClick = function (points, evt) {
                console.log(points, evt);
            };
        }
    ]
});
