'use strict';
angular.module('view.page.panels.panel-6', []);
angular.module('view.page.panels.panel-6').component('panel6', {
    templateUrl: 'views/pages/panels/panel-6/panel-6.html',
    controller: [
        function Panel6Controller() {
            var $ctrl = this;
            $ctrl.onInit = function () {
                console.log('dd');
                $ctrl.addClass('outer-element')
            };
        }
    ]
});
