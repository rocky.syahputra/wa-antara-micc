'use strict';
angular.module('view.page.panels.panel-3', []);
angular.module('view.page.panels.panel-3').component('panel3', {
    templateUrl: 'views/pages/panels/panel-3/panel-3.html',
    controller: [
        function Panel3Controller() {
            var $ctrl = this;
            $ctrl.onInit = function () {
                console.log('dd');
                $ctrl.addClass('outer-element')
            };
        }
    ]
});
