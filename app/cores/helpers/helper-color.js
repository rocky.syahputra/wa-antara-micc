'use strict';
angular.module('core.helper.color', []);
angular.module('core.helper.color').service('coreHelperColor', [
    function() {
        var service = this;
        service.randomizeColors = function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;
            while (0 !== currentIndex) {
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        };
        service.createColors = function (length) {
            var colors = [];
            var step = (length > 360 ? 1 : Math.ceil(360/length));
            var hue = 0;
            for(var i = 0; i < length; i++) {
                if (i % 360 === 0) {
                    hue = -1;
                }
                hue += step;
                colors.push('hsla('+ hue +', 85%, 65%, 1)');
            }
            return service.randomizeColors(colors);
        };
        return service;
    }
]);
