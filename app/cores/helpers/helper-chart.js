'use strict';
angular.module('core.helper.chart', []);
angular.module('core.helper.chart').service('coreHelperChart', ['coreHelperColor',
    function(coreHelperColor) {
        var service = {};
        service.createDatasets = function (data) {
            var length = data.length;
            var datasets = [];
            var colors = coreHelperColor.createColors(length);
            for(var i = 0; i < length; i++) {
                var options = {
                    fill: false,
                    lineTension: 0,
                    borderWidth: 3,
                    pointBackgroundColor: 'rgba(255,255,255,0)',
                    pointBorderColor: 'rgba(255,255,255,0)'
                };
                if(length > 6) { options.borderColor = colors[i]; options.backgroundColor = colors[i]; }
                datasets.push(options);
            }
            return datasets;
        };
        return service;
    }
]);
