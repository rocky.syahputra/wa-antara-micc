'use strict';
angular.module('micc', ['ngAnimate', 'ngRoute', 'ui.bootstrap', 'chart.js', 'core', 'view', 'model']);

// Generals
angular.module('micc').config(['$locationProvider',
    function config($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

// Chart.js
angular.module('micc').config(['ChartJsProvider',
    function (ChartJsProvider) {
        // Configure all charts
        ChartJsProvider.setOptions({
            responsive: true,
            maintainAspectRatio: false,
            defaultFontColor: 'rgba(254,254,254,1)',
            defaultFontFamily: '"proxima_nova_thin", Georgia, sans-serif',
            title: { display: false },
            tooltips: {
                enabled: true,
                mode: 'point',
                xPadding: 10,
                yPadding: 10,
                backgroundColor: 'rgba(16,19,38,1)',
                titleFontColor: 'rgba(254,254,254,1)',
                titleFontSize: 18,
                bodyFontColor: 'rgba(254,254,254,1)',
                bodyFontSize: 16
            },
            legend: {
                display: true,
                position: 'bottom',
                fullWidth: true,
                labels: {
                    fontColor: 'rgba(254,254,254,1)',
                    fontFamily: '"proxima_nova_thin", Georgia, sans-serif'
                }
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: false,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif'
                    },
                    ticks: {
                        display: true,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif'
                    },
                    gridLines: {
                        drawBorder: false,
                        offsetGridLines: true,
                        color: 'rgba(254,254,254,0.25)'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: false,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif'
                    },
                    ticks: {
                        display: true,
                        fontColor: 'rgba(254,254,254,0.25)',
                        fontFamily: '"proxima_nova_thin", Georgia, sans-serif'
                    },
                    gridLines: {
                        drawBorder: false,
                        offsetGridLines: true,
                        color: 'rgba(254,254,254,0.25)'
                    }
                }]
            },
            elements: {
                point: {
                    backgroundColor: 'rgba(255,255,255,0)',
                    borderColor: 'rgba(255,255,255,0)'
                }
            },
            chartColors: ['#2abc6e', '#ff0090', '#fa9315', '#63d6eb', '#fefefe', '#b6fb26']
        });
    }
]);

// Routers
angular.module('micc').config(['$routeProvider',
    function config($routeProvider) {
        $routeProvider.
        when('/panels', {
            template: '<panels></panels>'
        }).
        when('/panels/panel1', {
            template: '<panel1></panel1>'
        }).
        when('/panels/panel2', {
            template: '<panel2></panel2>'
        }).
        when('/panels/panel3', {
            template: '<panel3></panel3>'
        }).
        when('/panels/panel4', {
            template: '<panel4></panel4>'
        }).
        when('/panels/panel5', {
            template: '<panel5></panel5>'
        }).
        when('/panels/panel6', {
            template: '<panel6></panel6>'
        }).
        when('/panels/panel7', {
            template: '<panel7></panel7>'
        }).
        when('/panels/panel8', {
            template: '<panel8></panel8>'
        }).
        otherwise('/panels');
    }
]);
